import { NgForm } from '@angular/forms/src/directives';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AuthService} from '../auth.service';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SignupComponent implements OnInit {
 email: string = "";
 password: string ="";
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

signupForm(){

  this.authService.signupForm(this.email, this.password);
}
// signupForm(form: NgForm){

//   console.log("signup form");
//   console.log(form);

//    let email = form.value.email;
//    let password = form.value.password;

//   //  this.authService.signupForm(email, password);
//  }

}
