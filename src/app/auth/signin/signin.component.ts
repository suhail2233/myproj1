import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {AuthService} from '../auth.service';
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SigninComponent implements OnInit {
email: string = "";
 password: string ="";
  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

signinForm(){

  this.authService.signinForm(this.email, this.password);
}
}
