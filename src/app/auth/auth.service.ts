import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';


@Injectable()
export class AuthService {
  
  token: String = null;

  constructor(private router: Router) { }

signupForm(email, password){

  firebase.auth().createUserWithEmailAndPassword(email, password).then(
    (response) => {console.log(response)},
  ).catch(
    (error) => console.log(error)
  )
}

signinForm(email, password){
  firebase.auth().signInWithEmailAndPassword(email, password).then(
    (response) => {
      this.router.navigate(['/'])
      console.log('response start')
      // console.log(response)
      firebase.auth().currentUser.getToken().then(
        (token: string) => {
          this.token = token;
        }
      )
      console.log('response end')
    },
  ).catch(
    (error) => console.log(error)
  )
}


getToken(){
  // firebase.auth().currentUser.getToken().then(
  //   (token: string) => {
  //     this.token = token;
  //   }
  // )
  return this.token;
}

isAuthenticated():boolean{
  console.log('token is'  +this.token != undefined )
  return this.token != null;
}
}
