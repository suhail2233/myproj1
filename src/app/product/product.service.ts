import { AuthService } from '../auth/auth.service';
import { Headers, Http, RequestOptions } from '@angular/http';
import { LoggingService } from './logging.service';
import { Course } from '../core/model/course-model';
import { EventEmitter, Injectable } from '@angular/core';

@Injectable()
export class ProductService {

  courses: Array<Course> =[];

statusEventEmitter: EventEmitter<string> = new EventEmitter();

  constructor(private loggingService:LoggingService, private http: Http, private authService: AuthService) 
  { 
   let c1: Course = new Course(1, "MongoDB","assets/images/1MONGODB.jpg");
   let c2: Course = new Course(2, "AngularJS","assets/images/2ANGULARJS.png");
   let c3: Course = new Course(3, "NodeJS","assets/images/3NODEJS.png");
   let c4: Course = new Course(4, "JavaScript","assets/images/4JAVASCRIPT.jpg");

   this.courses.push(c1);
   this.courses.push(c2);
   this.courses.push(c3);
   this.courses.push(c4);


  }
  
  getCourses()
  {
    this.loggingService.log(this.courses);
    const token = this.authService.getToken();
    return this.http.get("https://my-porj1.firebaseio.com/product.json"+token);
    // return this.courses();
  }
 
  createCourse(course: Course[])
  {
    // this.courses.push(course);
    console.log("new courses are: " );
    console.log(this.courses)
    // return this.http.post("https://my-porj1.firebaseio.com/product.json", course);


    const header : Headers = new Headers({
        'Content-Type' : 'application/json'}
    );
      const options: RequestOptions = new RequestOptions();

      options.headers = header;
      return this.http.put("https://my-porj1.firebaseio.com/product.json", course, options)
  }

  getCourseById(id:number)
  {
    // for(var i = 0; i < this.courses.length; i++)
    //   {
    //     if(this.courses[i].id == id)
    //       {
    //         return this.courses[i];
    //       }
    //   }
    // return null;
      this.http.get("https://my-porj1.firebaseio.com/product/-KzwuOz73QMLUS0byn72.json").subscribe(
        (response) => {
          console.log("response is")
        console.log(response)
        },
      (error) => console.log(error)
      );
   return null;
  }
}
