//import { listenerCount } from 'cluster';
import { HttpModule } from '@angular/http';
import { LoggingService } from './logging.service';
import { FormsModule } from '@angular/forms';
import { ProductService } from './product.service';
import { Component, createComponent } from '@angular/compiler/src/core';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { CreateComponent } from './create/create.component';
import { MatCardModule, MatGridListModule, MatButtonModule, MatInputModule } from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import { CourseFormComponent } from './create/course-form/course-form.component';
import { DisplayCoursesComponent } from './create/display-courses/display-courses.component';
import { UtilModule } from "../util/util.module";

const routes : Routes=[
  {
    path: "",
    component: ProductComponent,
    children: 
    [
      {path:"", redirectTo: "list", pathMatch:"full"},
      {path:"create", component:CreateComponent},
      {path:"list", component:ListComponent},
      {path:"detail/:id", component:DetailComponent},  
    ]
  }
  
  
  //{path:"", redirectTo: "list", pathMatch:"full"},
  //{path:"create", component:CreateComponent},
  //{path:"list", component:ListComponent},
  //{path:"detail/:id", component:DetailComponent},

]

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes), MatCardModule, MatButtonModule, FormsModule,
     MatInputModule, MatExpansionModule, UtilModule, HttpModule
  ],
  declarations: [ProductComponent, ListComponent, DetailComponent, CreateComponent, CourseFormComponent, DisplayCoursesComponent],
  providers: [ProductService, LoggingService]
  // providers: []
})
export class ProductModule { }
