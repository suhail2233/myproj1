import { interval } from 'rxjs/observable/interval';
import { Observer, Subscription } from 'rxjs/Rx';
import { LoggingService } from '../logging.service';
import { ProductService } from '../product.service';
import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Course } from "../../core/model/course-model";
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  encapsulation: ViewEncapsulation.None,
  // providers: [ProductService]
})
export class ListComponent implements OnInit, OnDestroy {
  courses: Array<Course> =[];

  mySubsription: Subscription = new Subscription();

  constructor(private productService: ProductService,
  private loggingService: LoggingService) 
  { 
    // this.courses = productService.courses;
    this.loggingService.log(this.courses);

    let intervalObservable: Observable<number> = Observable.interval(2000);
    
     this.mySubsription = intervalObservable.subscribe(
      (data)=>{
        console.log("Data is " + data);
      },
      (error)=>{
        console.log("error is " + error);
      },
      ()=>{
        console.log("completed");
      }
    );


    let myCoursesObservable = Observable.create((observer : Observer<string>) => {
      setTimeout(() => {
        observer.next("First Course");
      }, 2000);
      setTimeout(() => {
        observer.next("Second Course");
      }, 4000);
      // setTimeout(() => {
      //   observer.error("Error Course");
      // }, 5000);
      // setTimeout(() => {
      //   observer.complete();
      // }, 6000);

      // setTimeout(() => {
      //   observer.next("Third course");
      // }, 8000);

      // setTimeout(() => {
      //   observer.error("ThirdError course");
      // }, 9000);
    });
    myCoursesObservable.subscribe(
      (data) => {
        console.log("Course is " + data);
      },
      (error) => {
        console.log("Error is " + error);
      },
      () => {
        console.log("Completed");
      }
    )

  }



  ngOnInit() 
  {

  }

  ngOnDestroy()
  {
    console.log("Destroy is called");
    this.mySubsription.unsubscribe();
  }
}
