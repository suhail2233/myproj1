import { Response } from '@angular/http';
import { error } from 'util';
import { AlertComponent } from '../../util/alert/alert.component';
import { ProductService } from '../product.service';
import { Course } from '../../core/model/course-model';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  encapsulation: ViewEncapsulation.None,
  // providers: [ProductService]
})
export class CreateComponent implements OnInit {
  courses: Course[] = [];
  
  @ViewChild(AlertComponent) alertComponent: AlertComponent;
  
  // newCourse: Course;

  // id: number;
  // name: string;
  // imgPath: String;

  constructor(private productService: ProductService) {
    this.getCourses();
   }

  ngOnInit() {
  }

 createCourse(event){
  // this.newCourse = new Course(this.id, this.name, this.imgPath);
  // console.log("new course is " + this.newCourse)
  // console.log(this.newCourse)
  // this.productService.createCourse(this.newCourse);
  // this.getCourses();
  console.log("inside parent and event is");
  console.log(event);
  this.productService.createCourse(event).subscribe(

    (response) => console.log(response),
    (error) => console.log(error)

  );
  // alert("course is  successfully added");
  this.alertComponent.successAlert("course is successfully added");
  // this.getCourses();
 }

 getCourses(){
  //  this.courses = this.productService.getCourses();
  this.productService.getCourses().subscribe(
    (response: Response) => {
      let data = response.json();
      
      console.log(data);
      // this.courses.push(data);
      this.courses = data;
    },
    (error) => console.log(error)

  );
 }

 getFacultyName(faculty){
  console.log("faculty is :" )
  console.log(faculty);
 }
}
