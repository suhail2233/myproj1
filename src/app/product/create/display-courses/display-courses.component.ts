import { Course } from '../../../core/model/course-model';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ProductService } from "../../product.service";

@Component({
  selector: 'app-display-courses',
  templateUrl: './display-courses.component.html',
  styleUrls: ['./display-courses.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [ProductService]
})
export class DisplayCoursesComponent implements OnInit, OnChanges {

 @Input("coursesData") childCourses: Course[];

  statusFromCreateComponent: string ="";
  constructor(private productService:ProductService) { 
    this.productService.statusEventEmitter.subscribe(

      (data) => {

        this.statusFromCreateComponent = data;
      }
    )//;
  }

  ngOnInit() {
  }
  
ngOnChanges(changes: SimpleChanges){
console.log("changes are");
console.log(changes);

}
}
