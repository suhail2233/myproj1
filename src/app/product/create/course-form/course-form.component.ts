import { ProductService } from '../../product.service';
import {
    AfterContentChecked,
    AfterContentInit,
    AfterViewChecked,
    AfterViewInit,
    Component,
    DoCheck,
    EventEmitter,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges,
    ViewEncapsulation
} from '@angular/core';
import { Course } from "../../../core/model/course-model";

@Component({
  selector: 'app-course-form',
  templateUrl: './course-form.component.html',
  styleUrls: ['./course-form.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class CourseFormComponent implements OnInit//, DoCheck, AfterContentChecked, AfterContentInit,
//AfterViewChecked, AfterViewInit, OnDestroy
{
  
  newCourse: Course;
  
  id: number;
  name: string;
  imagPath: string;
  
  @Output() getCourseData: EventEmitter<Course> = new EventEmitter();


  constructor(private productService:ProductService) { 
    console.log("constructor called!")
  }

  ngOnInit() {
    console.log("ngInit is called!")
  }

//   ngDoCheck(){
//   console.log("ngDoCheck is called");
// }
// ngAfterContentInit(){
//  console.log("ngAfterContentInit is called");
// }

// ngAfterContentChecked(){
//  console.log("ngAfterContentChecked is called");
// }
// ngAfterViewInit(){
//  console.log("ngAfterViewInit is called");
// }

// ngAfterViewChecked(){
//  console.log("ngAfterViewChecked is called");
// }

// ngOnDestroy(){
//  console.log("ngDestroy is called");
// }

createCourse(){
 this.newCourse = new Course(this.id, this.name, this.imagPath);
  console.log("new course is : " + this.newCourse);
  console.log(this.newCourse);
 this.getCourseData.emit(this.newCourse);

 this.sendStatusToDisplayCoomponent();
}

sendStatusToDisplayCoomponent(){
this.productService.statusEventEmitter.emit(this.name + " is  Created");
}

}
