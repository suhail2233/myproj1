import { ProductService } from '../product.service';
import { Course } from '../../core/model/course-model';
import { ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DetailComponent implements OnInit {
  id : number;
  name: string;
  faculty: string;
  course: Course;
  constructor(private activatedRoutes:ActivatedRoute, private productService:ProductService) 
  { 
    // this.id = this.activatedRoutes.snapshot.params['id'];
    this.name = this.activatedRoutes.snapshot.queryParams['name'];
    this.faculty =this.activatedRoutes.snapshot.queryParams['faculty'];

    this.activatedRoutes.queryParams.subscribe(
      (params: Params) => {
        this.name = params['name'];
      }
    );

    this.activatedRoutes.params.subscribe(
        (params : Params) => {
          alert("id is:" + params['id']);
          this.id = params['id'];
    }
    )

    this.getCourseById(this.id)
  }

  getCourseById(id)
  {
    // this.course = this.productService.getCourseById(id);
  }
  ngOnInit() {
  }

}
