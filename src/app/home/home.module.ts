import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { MatCardModule, MatButtonModule, MatInputModule, MatSelectModule, MatFormFieldModule, MatRadioModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule, 
    MatCardModule, 
    MatInputModule, 
    MatButtonModule, 
    FormsModule, 
    MatSelectModule, 
    MatRadioModule, 
    ReactiveFormsModule, 
    MatFormFieldModule
  ],
  declarations: [HomeComponent],
  exports: [HomeComponent ]
})
export class HomeModule { }
