//import { Observable } from 'rxjs/Rx';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Course } from '../core/model/course-model';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Observable } from "rxjs/Rx";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  userDataForm: FormGroup;

  usernameList = ['Lara', 'Sachin', 'Rohit'];
// @ViewChild('myForm') myForm: NgForm;
// question: string ="pet";
// genders: string[] = ['male', 'female'];
// gender: string = 'male';

  courses: Array<Course> =[];

  constructor() 
  { 
   let c1: Course = new Course(1, "Java","/assets/images/1.jpg");
   let c2: Course = new Course(2, "Spring","/assets/images/2.jpg");
   let c3: Course = new Course(3, "hibernate","/assets/images/3.jpg");

   this.courses.push(c1);
   this.courses.push(c2);
   this.courses.push(c3);

   
  }

  ngOnInit() {
    this.userDataForm = new FormGroup({
      'userData': new FormGroup({

        'username': new FormControl(null, [Validators.required, this.validateUserName.bind(this)]),
      'email': new FormControl(null, [Validators.required, Validators.email], this.asynchValidateUserName.bind(this)),

      }),
      'questions': new FormControl('school'),
      'hobbies': new FormArray([])

    });

  //   this.myForm.valueChanges.subscribe(
  //   (value) => console.log(value)
  //  );

  }


// submitForm(myForm: NgForm){
//   console.log(myForm);
// }


// setFormValue(){
//   this.myForm.setValue({
//     'answers':"",
//     "gender" :"female",
//     "questions":'',
//     "userData":{
//       "email":"a@a.com",
//       "username":"ABC"
//     }
//   });
// }

// setPacthValue(){

//   this.myForm.form.patchValue({
//     "userData" : {
//       "username": "test data"
//     }

//   });
// }

// resetForm(){
// this.myForm.reset();
// }

// submitForm(){
//   console.log(this.myForm);
// }

sumbitUserDataForm(){
  console.log(this.userDataForm);
}

addHobby(){
  (<FormArray>this.userDataForm.get('hobbies')).push(

    new FormControl(null)
  )
  console.log(this.userDataForm.get('hobbies'));
}

validateUserName(userNameControl: FormControl):{[key: string]:boolean}{
if(this.usernameList.indexOf(userNameControl.value) !== -1){
  return{'userNameIsAvailable':true}
}
 return null;
}

asynchValidateUserName(userNameControl: FormControl): Promise<any> | Observable<any>{
  const promise = new Promise<any>(
    (resolve, reject) => {
      setTimeout(
        () => {
          if(userNameControl.value === "test@test.com"){
            resolve({'emailIsAVailable':true})       
          }else{
            reject(null)
          }
        },3000
      )
    }
  );
  return promise;
}

}