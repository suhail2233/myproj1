import { AuthService } from './auth/auth.service';
import { CommonModule } from '@angular/common';
import { AboutUsModule } from './about-us/about-us.module';
import { HomeModule } from './home/home.module';
import { HomeComponent } from './home/home.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatButtonModule, MatCheckboxModule, MatSidenavModule, MatIconModule, MatInputModule } from '@angular/material';

import { AppComponent } from './app.component';
import { CoreModule } from "./core/core.module";
import { FormsModule } from "@angular/forms";
import { ContactUsModule } from "./contact-us/contact-us.module";
import { ContactUsComponent } from "./contact-us/contact-us.component";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
//import { AboutUsComponent } from "./about-us/about-us.component";


const routes: Routes = [
  {path: '', redirectTo : 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'sign-in', component: SigninComponent},
  {path: 'sign-up', component: SignupComponent},
  {path: 'product', loadChildren:"app/product/product.module#ProductModule"},
  {path: 'contact-us', loadChildren:"app/contact-us/contact-us.module#ContactUsModule"},
  {path: 'about-us', loadChildren:"app/about-us/about-us.module#AboutUsModule"},
  {path: '**', component:PageNotFoundComponent}
]


@NgModule({
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    SignupComponent,
    SigninComponent
  ],
  imports: [
    BrowserModule, 
    MatButtonModule, 
    MatCheckboxModule,
    MatIconModule, 
    BrowserAnimationsModule, 
    CoreModule, 
    FormsModule,  
    MatSidenavModule, 
    RouterModule.forRoot(routes), 
    HomeModule,
    CommonModule,
    RouterModule.forRoot(routes),
    MatInputModule

    //, ContactUsModule, AboutUsModule
    
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
