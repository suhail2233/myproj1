import { AuthService } from '../../../auth/auth.service';
import { Component, OnInit, ViewEncapsulation, Input} from '@angular/core';

@Component({
  selector: 'app-right-menu',
  templateUrl: './right-menu.component.html',
  styleUrls: ['./right-menu.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RightMenuComponent implements OnInit {

  @Input() childMenus: any;

  authService1: AuthService;

  constructor(private authService: AuthService) { 
    this.authService1 = authService;
  }

  ngOnInit() {
  }

}
