import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuModel } from "../model/menu-model";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {
  menus: Array<MenuModel> =[];
  constructor() 
  {
    let menu1 = new MenuModel("Home","/home");
    let menu2 = new MenuModel("About","/about-us");
    let menu3 = new MenuModel("OnlineClasses","laratechnology.com");
    // let menu4 = new MenuModel("sign-in","/sign-in");
    // let menu5 = new MenuModel("sign-up","/sign-up");
    let menu6 = new MenuModel("Students","laratechnology.com");
    let menu7 = new MenuModel("Product","/product");
    let menu8 = new MenuModel("ContactUs","/contact-us");
    let menu9 = new MenuModel("Our Recruiters","laratechnology.com");
    
    this.menus.push(menu1);
    this.menus.push(menu2);
    this.menus.push(menu3);
    // this.menus.push(menu4);
    // this.menus.push(menu5);
    this.menus.push(menu6);
    this.menus.push(menu7);
    this.menus.push(menu8);
    this.menus.push(menu9);

  }
folders = [
    {
      name: 'Photos',
      updated: new Date('1/1/16'),
    },
    {
      name: 'Recipes',
      updated: new Date('1/17/16'),
    },
    {
      name: 'Work',
      updated: new Date('1/28/16'),
    }
  ];
  notes = [
    {
      name: 'Vacation Itinerary',
      updated: new Date('2/20/16'),
    },
    {
      name: 'Kitchen Remodel',
      updated: new Date('1/18/16'),
    }
  ];
  ngOnInit() {
  }

}
