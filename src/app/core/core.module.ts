import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MatSidenavModule, MatToolbarModule, MatIconModule, MatListModule, MatButtonModule, MatChipsModule } from '@angular/material';
import { RightMenuComponent } from './header/right-menu/right-menu.component';

@NgModule({
  imports: [
    CommonModule,MatSidenavModule,MatToolbarModule,MatIconModule,MatListModule,
    MatButtonModule,MatChipsModule, RouterModule

  ],
  declarations: [HeaderComponent, FooterComponent, RightMenuComponent],
  exports: [HeaderComponent, FooterComponent]
})
export class CoreModule { }
