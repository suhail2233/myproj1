import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us.component';
import { AboutUs1Component } from './about-us1/about-us1.component';

const routes: Routes = [
{
  //path:"", component:AboutUsComponent
  path:"", redirectTo:"about", pathMatch: "full"
},
{
  path:"about", component: AboutUsComponent

},
{
  path:"about-us-1", component: AboutUs1Component

}
]

@NgModule({
  imports: [
    CommonModule, RouterModule.forChild(routes)
  ],
  declarations: [AboutUsComponent, AboutUs1Component],
  //exports: [AboutUsComponent, AboutUs1Component]
})
export class AboutUsModule { }
