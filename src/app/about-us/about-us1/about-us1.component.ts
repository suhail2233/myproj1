import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-about-us1',
  templateUrl: './about-us1.component.html',
  styleUrls: ['./about-us1.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AboutUs1Component implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
